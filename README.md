# Teste Front-End
# Olá desenvolvedor Front-end, siga abaixo as instruções do teste.

### Tecnologias
 - Um framework JavaScript podendo optar por: [Vue.js](https://vuejs.org/v2/guide/), [ReactJS](https://reactjs.org/docs/getting-started.html) ou [Angular](https://angular.io/guide/quickstart)
 - Um pré-processador CSS ([SASS](https://sass-lang.com) ou [LESS](http://lesscss.org))
 - Utilizar HTML5 com a semântica correta.
 
### Desenvolvimento
 - A tela deve ser uma cópia fiel do Layout passado: [https://bit.ly/2Lyo7BC](https://bit.ly/2Lyo7BC)
 - Segue uma imagem com os ícones usados no layout: [https://bit.ly/2LyzPMI](https://bit.ly/2LyzPMI).
 
### Requisitos
 - Deve conter o filtro de preço funcionando.
 - Deve ser responsivo (O Layout para outras plataformas fica a critério do candidato, utilizando sempre os conceitos de UX e UI).
 - Deve ter dois tipos de resultado, linha e bloco.

### Bônus
 - Utilizar alguma metodologia de nomenclatura css (OOCSS, SMACSS, BEM ou DRY)
 - Utilizar conceito mobile-first.

### Entrega
 - Crie um fork desse repo, tutorial: [https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
 - Faça um pull-request, tutorial: [https://confluence.atlassian.com/bitbucket/create-a-pull-request-to-merge-your-change-774243413.html](https://confluence.atlassian.com/bitbucket/create-a-pull-request-to-merge-your-change-774243413.html)
